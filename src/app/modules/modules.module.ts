import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulesRoutingModule } from './modules-routing.module';
import { AccomodationComponent } from './accomodation/accomodation.component';
import { CityComponent } from './city/city.component';
import { PlansComponent } from './plans/plans.component';
import { CardsComponent } from './cards/cards.component';


@NgModule({
  declarations: [AccomodationComponent,CityComponent,PlansComponent,CardsComponent],
  imports: [
    CommonModule,
    ModulesRoutingModule
  ],
  exports:[
    AccomodationComponent,
    CityComponent,
    PlansComponent,
    CardsComponent
  ]
})
export class ModulesModule { }
