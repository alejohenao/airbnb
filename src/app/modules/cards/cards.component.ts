import { Component, Input,OnInit } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.styl']
})
export class CardsComponent implements OnInit {
  @Input () 
  service: any[];

  constructor() { }

  ngOnInit(): void {
  }

}
