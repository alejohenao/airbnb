import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-accomodation',
  templateUrl: './accomodation.component.html',
  styleUrls: ['./accomodation.component.styl']
})
export class AccomodationComponent implements OnInit {

  
  dataList = [
    {
     title: '',
     description:'Alojamientos enteros',
     image:'../../../assets/images/image1.jpeg'
    },    
    {
      title: '',
      description:'Alojamientos únicos',
      image:'../../../assets/images/image7.jpeg'
    },
    {
      title: '',
      description:'Cabañas y casas de campo',
      image:'../../../assets/images/image8.jpeg'
     },
     {
       title: '',
       description:'Se admiten mascotas',
       image:'../../../assets/images/image4.jpeg'
      }    
  ];
  constructor() { }
  @Input () card:string;
  ngOnInit(): void {
  }

}
