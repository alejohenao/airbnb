import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.styl']
})
export class CityComponent implements OnInit {

  cityList = [
    {
     title: 'Melgar',
     description:'2 horas en auto',
     image:'../../../assets/images/image5.jpeg'

    },
    {
      title: 'Cali',
      description:'4,5 horas en auto',
      image:'../../../assets/images/image6.jpeg'
    },
    {
      title: 'Guatape',
      description:'6 horas en auto',
      image:'../../../assets/images/image9.jpeg'

    },
    {
      title: 'Neiva',
      description:'3,5 horas en auto',
      image:'../../../assets/images/image10.jpeg'

    },
    {
      title: 'Salento',
      description:'2 horas en auto',
      image:'../../../assets/images/image11.jpeg'

    },
    {
      title: 'Carmen de Apicalá',
      description:'2 horas en auto',
      image:'../../../assets/images/image12.jpeg'

    },
    {
      title: 'Honda',
      description:'2 horas en auto',
      image:'../../../assets/images/image2.jpeg'

    },
    {
      title: 'Ibagué',
      description:'15 minutos en auto',
      image:'../../../assets/images/image3.jpeg'

    }    
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
