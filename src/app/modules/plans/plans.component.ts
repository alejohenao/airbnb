import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.styl']
})
export class PlansComponent implements OnInit {

  plansList = [
    {
      title: 'Experiencias en linea',
      description:'Descubre el mundo sin salir de casa.',
      image:'../../../assets/images/image15.jpeg'
     },
    {
     title: 'Experiencias',
     description:'Actividades estés donde estés.',
     image:'../../../assets/images/image16.jpeg'
    },
    {
      title: 'Aventuras',
      description:'Viajes de varios días con alojamiento y comidas.',
      image:'../../../assets/images/image13.jpeg'
    }
    
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
