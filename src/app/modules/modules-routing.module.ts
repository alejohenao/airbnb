import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccomodationComponent } from './accomodation/accomodation.component';
import { CityComponent } from './city/city.component';
import { PlansComponent } from './plans/plans.component';


const routes: Routes = [
  {
    path: 'accomodation',
    component: AccomodationComponent
  },
  {
    path:'city',
    component:CityComponent
  },
  {
    path:'plans',
    component:PlansComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
