import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AccomodationComponent } from '../modules/accomodation/accomodation.component';
import { CityComponent } from '../modules/city/city.component';
import { PlansComponent } from '../modules/plans/plans.component';


const routes: Routes = [  
  {
    path:'dashboard',
    loadChildren:() => import ('../dashboard/dashboard.module') 
    .then(m => m.DashboardModule)
  },
  {
    path:'',
    component:MainComponent,    
    children:[
      {
        path:'accomodation',
        component:AccomodationComponent
      },
      {
        path:'city',
        component:CityComponent
      },
      {
        path:'plans',
        component:PlansComponent
      }
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

