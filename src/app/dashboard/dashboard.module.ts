import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from './header/header.component';
import { ModulesModule } from '../modules/modules.module';


@NgModule({
  declarations: [MainComponent,NavbarComponent, FooterComponent,  HeaderComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ModulesModule
  ],
  exports:[
    ModulesModule
  ]
})

export class DashboardModule { }
